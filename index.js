const express = require('express');
const indexRouter = require('./routes/server-controller');
const usuarioRouter = require('./routes/usuario-controller');
const anfitrionRouter = require('./routes/vista-anfitrion-controller');
const viajerosRouter = require('./routes/vista-viajeros-controller');
const comentarioRouter = require('./routes/comentario-controller');
const fotosRouter = require('./routes/publica-fotos-controller');

const logger = require('morgan');
const cors = require('cors');
const app = express();

app.use(express.json());

app.use("/img", express.static('uploads'));
app.use(logger('dev'));

app.use(cors());

app.use('/', indexRouter);
app.use('/usuarios', usuarioRouter);
app.use('/anfitriones', anfitrionRouter);
app.use('/viajeros', viajerosRouter);
app.use('/comentarios', comentarioRouter);
app.use('/fotos', fotosRouter);

const port = 3000;
app.listen(port, ()=>console.log("Listening on port "+port));
