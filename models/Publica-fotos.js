'use strict';

module.exports = (sequelize, DataTypes) => {

    const Publica_fotos = sequelize.define('Publica_fotos', {

        url : DataTypes.STRING,
        pais_id : DataTypes.INTEGER,
        usuario_id : DataTypes.INTEGER,

    }, {tableName: 'foto', timestamps: false});

    return Publica_fotos;
}

